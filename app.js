const express = require('express');
const { initDb } = require('./config/dbConfig');

initDb();


const app = express();
app.use(express.json());

const usersRoute = require('./routes/users');
const carsRoute = require('./routes/cars');

app.use('/api/users', usersRoute.route);
app.use('/api/cars', carsRoute.router);


app.listen('3000', () => {
    console.log('Server running on port 3000');
});

