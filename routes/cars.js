const express = require('express');
const router = express.Router();

const { getDb } = require('../config/dbConfig');

const db = getDb();


router.post('/', (req, res) => {
    const sqlQuery = 'INSERT INTO cars SET ?';
    return db.query(sqlQuery, req.body, (error, result) => {
        if (error) {
            res.send(error);
        }
        res.send('New Car is added');
    });
});

router.get('/search-cars', (req, res) => {
    const sqlQuery = `SELECT * FROM cars WHERE id NOT IN (SELECT carId FROM bookings b WHERE '${req.body.fromDateTime}' NOT BETWEEN b.bookingFrom AND b.bookedUpto)`;
    return db.query(sqlQuery, (error, result) => {
        if (error) {
            res.send(error);
        }
        res.send(result);
    });
});

router.get('/calculate-price', (req, res) => {
    const sqlQuery = `SELECT * FROM cars WHERE id = ${req.body.carId}`;
    return db.query(sqlQuery, (error, result) => {
        if (error) {
            res.send(error);
        }
        if (result.length) {
            const fromDate = new Date(req.body.fromDateTime);
            const toDate = new Date(req.body.toDateTime);
            const { pph, basePrice, securityDeposit } = result[0]; // Since we are passing the id which is unique in the table we will receive only 1 entry in the result
            const totalAmount = basePrice + securityDeposit + (pph * new Date(toDate - fromDate).getUTCHours());
            res.send(`Amount payable is INR. ${totalAmount}`);
        }
        else {
            res.send('Oops Something went wrong');
        }
    });
});


router.get('/car/bookings', (req, res) => {
    const sqlQuery = `SELECT u.firstName, u.lastName, b.totalBookingDurationInHours, b.totalAmountToPay FROM bookings b JOIN users u on u.userId = b.userId WHERE carId = ${req.body.carId}`;
    return db.query(sqlQuery, (error, result) => {
        if (error) {
            res.send(error);
        }
        if (result.length) {
            res.send(result);
        }
        else {
            res.send(`No bookings found for carId: ${req.body.carId}`);
        }
    });
});


router.post('/car/book', (req, res) => {
    const sqlQuery = `SELECT * FROM cars WHERE id = ${req.body.carId}`;
    return db.query(sqlQuery, (error, result) => {
        if (error) {
            res.send(error);
        }
        if (result.length) {
            const carData = result[0];
            const totalBookingDurationInHours = new Date(new Date(req.body.toDateTime) - new Date(req.body.fromDateTime)).getUTCHours();
            const totalAmountToPay = carData.basePrice + carData.securityDeposit + (carData.pph * totalBookingDurationInHours);
            const bookingFrom = req.body.fromDateTime;
            const bookedUpto = req.body.toDateTime;
            delete req.body.fromDateTime;
            delete req.body.toDateTime;
            const bookingData = {
                ...req.body,
                bookingFrom,
                bookedUpto,
                totalBookingDurationInHours,
                totalAmountToPay,
            };
            const insertSqlQuery = `INSERT INTO bookings SET ?`;
            db.query(insertSqlQuery, bookingData, (error, result) => {
                if (error) {
                    res.send(error);
                }
                res.send('Booking confirm');
            });
        }
        else {
            res.send('Oops Something went wrong');
        }
    });
});

module.exports = {
    router
};