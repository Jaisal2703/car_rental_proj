const express = require('express');
const route = express.Router();

const { getDb } = require('../config/dbConfig');

const db = getDb();


route.post('/', (req, res) => {
    const sqlQuery = 'INSERT INTO users SET ?';
    return db.query(sqlQuery, req.body, (error, result) => {
        if (error) {
            res.send(error);
        }
        res.send('User created');
    });
});

route.get('/user/bookings', (req, res) => {
    const sqlQuery = `SELECT c.carLicenseNumber, c.manufacturer, c.model, b.totalBookingDurationInHours, b.totalAmountToPay FROM cars c JOIN bookings b ON c.id = b.carId WHERE b.userId = ${req.body.userId}`;
    return db.query(sqlQuery, (error, result) => {
        if (error) {
            res.send(error);
        }
        if (result.length) {
            res.send(`Cars booked by user till date \n${JSON.stringify(result)}`);
        }
        else {
            res.send(`No bookings found for given user`);
        }
    });
});

module.exports = {
    route
};