/* eslint-disable no-undef */
const { getDb } = require('../config/dbConfig');
const db = getDb();

async function createTableInDatabase(db, tableName, tableColumns, path) {
  try {
    await db.query(`DROP TABLE IF EXISTS ${tableName} CASCADE`);
    await db.query(`CREATE TABLE ${tableName} ${tableColumns}`);
    if (path) {
      await db.query(`LOAD DATA LOCAL INFILE ${path} INTO TABLE ${tableName} FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS`);
    }
  } catch (e) {
    console.error(e);
  }
}


// Calling the functions to create the table by passing required parameters
async function callAll(db) {
  // Create users table with some default data
  await createTableInDatabase(
    db,
    'users',
    `(
      userId INT AUTO_INCREMENT UNIQUE,
      firstName VARCHAR(225) NOT NULL,
      lastName VARCHAR(225) NOT NULL,
      mobileNo VARCHAR(225) NOT NULL,
      access VARCHAR(225) NOT NULL,
      PRIMARY KEY(userId)
    )`,
    `'${process.cwd()}/data/UsersData.csv'`
  );

  // Create cars table with some default data
  await createTableInDatabase(
    db,
    'cars',
    `(
      id INT AUTO_INCREMENT UNIQUE,
      carLicenseNumber VARCHAR(225) UNIQUE,
      manufacturer VARCHAR(225),
      model VARCHAR(225),
      basePrice INT,
      pph INT,
      securityDeposit INT,
      PRIMARY KEY(id)
    )`,
    `'${process.cwd()}/data/CarsData.csv'`
  );

  await createTableInDatabase(
    db,
    'bookings',
    `(
      id INT AUTO_INCREMENT UNIQUE,
      carId INT NOT NULL,
      userId INT NOT NULL,
      bookingFrom TIMESTAMP NOT NULL,
      bookedUpto TIMESTAMP NOT NULL,
      totalBookingDurationInHours INT NOT NULL,
      totalAmountToPay INT NOT NULL,
      CONSTRAINT fk_cars FOREIGN KEY(carId)
      REFERENCES cars(id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
      CONSTRAINT fk_users FOREIGN KEY(userId)
      REFERENCES users(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
      PRIMARY KEY(id)
    )`
  );

  await db.end();
}

callAll(db);
