const { getDb } = require('../config/dbConfig');

const db = getDb();

function createNewUserEntry(userData) {
    const sqlQuery = 'INSERT INTO users SET ?';
    return db.query(sqlQuery, userData, (error, result) => {
        if (error) throw error;
    });
}

module.exports = {
    createNewUserEntry
};
