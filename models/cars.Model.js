const { getDb } = require('../config/dbConfig');

const db = getDb();

function createNewEntryInCars(carData) {
    const sqlQuery = 'INSERT INTO cars SET ?';
    return db.query(sqlQuery, carData, (error, result) => {
        if (error) throw error;
    });
}

function newBooking(bookingData) {
    const sqlQuery = 'INSERT INTO bookings SET ?';
    return db.query(sqlQuery, bookingData, (error, result) => {
        if (error) throw error;
    });
}

function findCarById(carId) {
    const r = db.query(`SELECT * FROM cars WHERE id = ?`, carId, (err, res, fields) => {
        console.log('-----', res);
        // return res;
    });
    return r;
}

module.exports = {
    createNewEntryInCars,
    findCarById
}