

const mysql = require('mysql');

// Create a connection to mysql db
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'jaisal2703',
    database: 'carrentalsystem',
});

// Connect
function initDb() {
    db.connect((error) => {
        if (error) {
            throw error;
        }
        console.log('Connected to mysql now...');
    });

}

function getDb() {
    return db;
}

module.exports = {
    initDb,
    getDb
}
